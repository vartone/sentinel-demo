package org.example.email.api;

/**
 * @author lihuazeng
 */
public interface MailSenderService {
    /**
     * 测试服务限流
     * @param email
     * @param emailEntry
     * @return
     */
    boolean send(String email, String emailEntry);

    /**
     * 测试服务熔断
     * @return
     */
    String breaker();
}
