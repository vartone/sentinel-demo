package org.example.email.service;

import org.apache.dubbo.config.annotation.Service;
import org.example.email.api.MailSenderService;

import java.util.concurrent.ThreadLocalRandom;


/**
 * @author lihuazeng
 */
@Service(version = "1.0.0", protocol = "${dubbo.protocols.dubbo.name}")
public class MailSenderServiceImpl implements MailSenderService {

    @Override
    public boolean send(String email, String emailEntry) {
        System.out.println("发送邮件到邮箱："+email+"。内容："+emailEntry);
        return true;

    }

    @Override
    public String breaker() {
        int c = ThreadLocalRandom.current().nextInt(9, 15);
        try {
            // 模拟业务时间
            Thread.sleep(c);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // 正常数据，非熔断
        return "normal";
    }

}
