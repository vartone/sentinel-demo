package com.chatly.sentinel.demo.config;

import com.alibaba.csp.sentinel.datasource.Converter;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRule;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * 限流配置
 *
 * @author lihuazeng
 */
@Configuration
public class FlowRuleConfig {
    @Bean
    public Converter<String, List<FlowRule>> converter() {
        return source -> JSON.parseObject(source, new TypeReference<List<FlowRule>>(){});
    }
}
