package com.chatly.sentinel.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Collections;

@SpringBootApplication
public class SentinelWebDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SentinelWebDemoApplication.class, args);
    }

}
