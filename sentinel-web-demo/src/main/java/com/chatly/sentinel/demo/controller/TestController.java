package com.chatly.sentinel.demo.controller;

import com.chatly.sentinel.demo.service.TestService;
import org.apache.dubbo.config.annotation.Reference;
import org.example.email.api.MailSenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lihuazeng
 */
@RestController
public class TestController {
    @Autowired
    private TestService testService;

    /**
     * 测试web层限流
     * @return
     */
    @GetMapping("/hello")
    public String hello() {
        return "Hello";
    }

    /**
     * 测试服务层限流
     * @return
     */
    @GetMapping("/slow")
    public String slow() {
        testService.send("2342@qq.com");
        return "slow";
    }

    /**
     * 测试服务层熔断
     * @return
     */
    @GetMapping("/breaker")
    public String breaker() {
        return testService.breaker();
    }
}
