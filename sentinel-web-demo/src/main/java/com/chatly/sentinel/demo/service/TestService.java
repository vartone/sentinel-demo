package com.chatly.sentinel.demo.service;

import org.apache.dubbo.config.annotation.Reference;
import org.example.email.api.MailSenderService;
import org.springframework.stereotype.Service;

@Service
public class TestService {
    @Reference(version = "1.0.0",
            application = "${spring.application.name}",
            url = "dubbo://localhost:20080?version=1.0.0", timeout = 30000)
    private MailSenderService mailSenderService;


    public String send(String name) {
        mailSenderService.send(name, "111");
        return "success";
    }

    public String breaker() {
        return mailSenderService.breaker();
    }
}
